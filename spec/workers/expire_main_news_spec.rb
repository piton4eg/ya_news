require 'rails_helper'

RSpec.describe ExpireMainNews, type: :worker do
  describe '.perform' do
    subject { described_class.new }

    before do
      allow_any_instance_of(Yandex).to receive(:first_news)
        .and_return(title: 'Post', annotation: 'Annotation')
    end

    context 'when there is custom post' do
      before do
        create(:post, :custom)
      end

      it 'does not create post' do
        expect { subject.perform }.not_to change(Post, :count)
      end

      it 'does not broadcast post' do
        expect(ActionCable).not_to receive(:server)
        subject.perform
      end
    end

    context 'when there is not custom post' do
      it 'fetch yandex news' do
        expect_any_instance_of(Yandex).to receive(:first_news)
        subject.perform
      end

      it 'creates post' do
        expect { subject.perform }.to change(Post, :count).by(1)
      end

      it 'broadcasts new post' do
        server = double
        expect(ActionCable).to receive(:server).and_return(server)
        expect(server).to receive(:broadcast)

        subject.perform
      end
    end
  end
end
