require 'rails_helper'

RSpec.describe YandexNews, type: :worker do
  describe '.perform' do
    subject { described_class.new }

    before do
      allow_any_instance_of(Yandex).to receive(:first_news)
        .and_return(title: 'Post', annotation: 'Annotation')
    end

    it 'fetch yandex news' do
      expect_any_instance_of(Yandex).to receive(:first_news)
      subject.perform
    end

    it 'creates post' do
      expect { subject.perform }.to change(Post, :count).by(1)
    end

    it 'broadcasts new post' do
      server = double
      expect(ActionCable).to receive(:server).and_return(server)
      expect(server).to receive(:broadcast)

      subject.perform
    end

    context 'when post exists already' do
      before do
        Post.create(title: 'Post', annotation: 'Annotation')
      end

      it 'does not create duplicate post' do
        expect { subject.perform }.not_to change(Post, :count)
      end

      it 'does not broadcast post' do
        expect(ActionCable).not_to receive(:server)
        subject.perform
      end
    end
  end
end
