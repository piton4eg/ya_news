require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  subject { JSON(response.body).with_indifferent_access }

  let(:custom_post) { create :post, :custom }
  let(:yandex_post) { create :post, :from_yandex }

  describe 'GET #main' do
    it 'returns 200' do
      get :main
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    context 'when custom post exists' do
      before do
        custom_post
        yandex_post
      end

      it 'returns custom post' do
        get :main
        is_expected.to include(
          title: 'Custom',
          annotation: 'Custom Annotation',
          custom: true
        )
      end
    end

    context 'when custom post does not exist' do
      before do
        yandex_post
      end

      it 'returns not custom post' do
        get :main
        is_expected.to include(
          title: 'Yandex',
          annotation: 'Yandex Annotation',
          custom: false
        )
      end
    end

    context 'when there are no posts' do
      it 'returns empty json' do
        get :main
        is_expected.to eq({})
      end
    end
  end

  describe 'GET #edit' do
    it 'returns 200' do
      get :edit
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    context 'when custom post exists' do
      before do
        custom_post
      end

      it 'returns custom post' do
        get :edit
        is_expected.to include(
          title: 'Custom',
          annotation: 'Custom Annotation',
          custom: true
        )
      end
    end

    context 'when custom post does not exist' do
      before do
        yandex_post
      end

      it 'returns not custom post' do
        get :edit
        is_expected.to eq({})
      end
    end
  end

  describe 'PUT #update' do
    context 'when post valid' do
      it 'returns 200' do
        put :update, params: { id: custom_post.id, title: 'New Title' }
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it 'updates post' do
        put :update, params: { id: custom_post.id, title: 'New Title' }
        expect(custom_post.reload.title).to eq 'New Title'
      end

      it 'returns updated custom post' do
        put :update, params: { id: custom_post.id, title: 'New Title' }
        is_expected.to include(
          title: 'New Title',
          annotation: 'Custom Annotation',
          custom: true
        )
      end

      it 'broadcasts post' do
        server = double
        expect(ActionCable).to receive(:server).and_return(server)
        expect(server).to receive(:broadcast).with('post_channel', post: custom_post)

        put :update, params: { id: custom_post.id, title: 'New Title' }
      end

      it 'schedules expiration' do
        expect(ExpireMainNews).to receive(:perform_at).with(custom_post.main_until)

        put :update, params: { id: custom_post.id, title: 'New Title' }
      end
    end

    context 'when post invalid' do
      it 'returns 422' do
        put :update, params: { id: custom_post.id, title: '' }
        expect(response).not_to be_success
        expect(response).to have_http_status(422)
      end

      it 'does not update post' do
        put :update, params: { id: custom_post.id, title: '' }
        expect(custom_post.reload.title).to eq 'Custom'
      end

      it 'returns errors for title' do
        put :update, params: { id: custom_post.id, title: '' }
        expect(subject['errors']).to include :title
      end

      it 'does not broadcast post' do
        expect(ActionCable).not_to receive(:server)
        put :update, params: { id: custom_post.id, title: '' }
      end

      it 'does not schedule expiration' do
        expect(ExpireMainNews).not_to receive(:perform_at)
        put :update, params: { id: custom_post.id, title: '' }
      end
    end
  end

  describe 'POST #create' do
    let(:post_params) do
      {
        title: 'New Post',
        annotation: 'Annotation',
        main_until: DateTime.current + 1.hour
      }
    end

    context 'when post valid' do
      it 'returns 200' do
        post :create, params: post_params
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it 'creates post' do
        post :create, params: post_params
        expect(Post.custom).to have_attributes(
          title: 'New Post',
          annotation: 'Annotation'
        )
      end

      it 'returns new post' do
        post :create, params: post_params
        is_expected.to include(
          title: 'New Post',
          annotation: 'Annotation',
          custom: true
        )
      end

      it 'broadcasts post' do
        server = double
        expect(ActionCable).to receive(:server).and_return(server)
        expect(server).to receive(:broadcast)

        post :create, params: post_params
      end

      it 'schedules expiration' do
        expect(ExpireMainNews).to receive(:perform_at)

        post :create, params: post_params
      end
    end

    context 'when post invalid' do
      it 'returns 422' do
        post :create, params: { title: 'New Post' }
        expect(response).not_to be_success
        expect(response).to have_http_status(422)
      end

      it 'does not create post' do
        expect {
          post :create, params: { title: 'New Post' }
        }.not_to change(Post, :count)
      end

      it 'returns errors for title' do
        post :create, params: { title: 'New Post' }
        expect(subject['errors']).to include :annotation, :main_until
      end

      it 'does not broadcast post' do
        expect(ActionCable).not_to receive(:server)
        post :create, params: { title: 'New Post' }
      end

      it 'does not schedule expiration' do
        expect(ExpireMainNews).not_to receive(:perform_at)
        post :create, params: { title: 'New Post' }
      end
    end
  end
end
