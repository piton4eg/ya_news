FactoryGirl.define do
  factory :post do
    trait :from_yandex do
      custom { false }
      title 'Yandex'
      annotation 'Yandex Annotation'
    end

    trait :custom do
      custom { true }
      title 'Custom'
      annotation 'Custom Annotation'
      main_until { DateTime.current + 1.hour }
    end
  end
end
