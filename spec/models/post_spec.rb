require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:custom_post) { create :post, :custom }
  let(:yandex_post) { create :post, :from_yandex }

  describe '.main' do
    subject { described_class.main }

    context 'when custom post exists' do
      before do
        custom_post
        yandex_post
      end

      it { is_expected.to eq custom_post }
    end

    context 'when custom post does not exist' do
      before do
        yandex_post
      end

      it { is_expected.to eq yandex_post }
    end

    context 'when there are no posts' do
      it { is_expected.to be_nil }
    end
  end

  describe '.custom' do
    subject { described_class.custom }

    before do
      custom_post
    end

    it 'returns last custom post' do
      last_post = create(:post, :custom)
      is_expected.to eq last_post
    end

    it 'does not return not custom post' do
      last_post = create(:post, :from_yandex)
      is_expected.to eq custom_post
    end
  end

  describe '.yandex' do
    subject { described_class.yandex }

    before do
      yandex_post
    end

    it 'returns last yandex post' do
      last_post = create(:post, :from_yandex)
      is_expected.to eq last_post
    end

    it 'does not return custom post' do
      last_post = create(:post, :custom)
      is_expected.to eq yandex_post
    end
  end
end
