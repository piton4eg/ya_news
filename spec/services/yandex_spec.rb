require 'rails_helper'

RSpec.describe Yandex do
  describe '#first_news' do
    subject { described_class.new.first_news }

    it 'returns attributes for post' do
      VCR.use_cassette('yandex') do
        is_expected.to eq(
          title: "Экс-супруга Потанина подала к нему новый иск на 850 миллиардов рублей",
          annotation: "Бывшая супруга президента &quot;Норильского никеля&quot; Владимира Потанина подала иск к миллиардеру на 849,2 миллиарда рублей, сообщает Forbes со ссылкой на копию заявления, которая есть в распоряжении издания.",
          date: "2017-05-23 00:24:51.000000000 +0300"
        )
      end
    end
  end
end
