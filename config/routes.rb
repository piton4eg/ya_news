require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  mount ActionCable.server, at: '/cable'

  scope '/api' do
    resources :posts, only: [:update, :create] do
      collection do
        get :edit, :main
      end
    end
  end
end
