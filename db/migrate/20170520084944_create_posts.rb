class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title, null: false
      t.text :annotation, null: false
      t.datetime :date
      t.datetime :main_until

      t.timestamps
    end
  end
end
