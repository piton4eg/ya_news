class AddCustomToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :custom, :boolean, default: false, null: false
  end
end
