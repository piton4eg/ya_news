# Yandex News

Setup:
- install and run postgresql
- install and run redis
- bundle install && cd ui && yarn install && cd ..
- bundle exec rake db:create db:migrate
- rake start (it runs foreman)
- open http://localhost:3000/
