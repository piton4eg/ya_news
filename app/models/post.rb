class Post < ActiveRecord::Base
  validates :title, :annotation, presence: true
  validates :main_until, presence: true, if: :custom?
  validate :main_until_in_future, if: :custom?

  def self.main
    custom || yandex
  end

  def self.yandex
    where(custom: false).last
  end

  def self.custom
    where(custom: true).where('main_until > ?', DateTime.current).last
  end

  private def main_until_in_future
    if main_until.present? && main_until < DateTime.current
      errors.add(:main_until, "can't be in past")
    end
  end
end
