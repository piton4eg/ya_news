class PostsController < ApplicationController
  def main
    render json: Post.main.as_json || {}
  end

  def edit
    render json: Post.custom.as_json || {}
  end

  def update
    post = Post.find(params[:id])
    if post.update(post_params)
      handle_post(post)
      render json: post.as_json
    else
      render json: { errors: post.errors.messages }, status: :unprocessable_entity
    end
  end

  def create
    post = Post.new(post_params.merge(custom: true))
    if post.save
      handle_post(post)
      render json: post.as_json
    else
      render json: { errors: post.errors.messages }, status: :unprocessable_entity
    end
  end

  private def post_params
    params.permit(:title, :annotation, :main_until)
  end

  private def handle_post(post)
    broadcast_post(post)
    schedule_expiration(post)
  end

  private def broadcast_post(post)
    ActionCable.server.broadcast 'post_channel', post: post
  end

  private def schedule_expiration(post)
    ExpireMainNews.perform_at(post.main_until)
  end
end
