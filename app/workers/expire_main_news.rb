require 'sidekiq-scheduler'

class ExpireMainNews
  include Sidekiq::Worker

  def perform
    if Post.custom.blank?
      yandex_news = Yandex.new.first_news

      yandex_post = Post.yandex || Post.new
      yandex_post.update(yandex_news)

      ActionCable.server.broadcast 'post_channel', post: yandex_post
    end
  end
end
