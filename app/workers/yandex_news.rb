require 'sidekiq-scheduler'

class YandexNews
  include Sidekiq::Worker

  def perform
    yandex_news = Yandex.new.first_news

    yandex_post = Post.yandex || Post.new
    yandex_post.assign_attributes(yandex_news)

    if yandex_post.changed? && Post.custom.blank?
      yandex_post.save
      ActionCable.server.broadcast 'post_channel', post: yandex_post
    end
  end
end
