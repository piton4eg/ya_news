require 'rss'
require 'open-uri'

class Yandex
  YANDEX_RSS = 'https://news.yandex.ru/index.rss'

  def first_news
    result = RSS::Parser.parse(open(YANDEX_RSS).read, false).items[4]

    {
      title: result.title,
      annotation: result.description,
      date: result.pubDate
    }
  end
end
