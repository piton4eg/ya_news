import { combineReducers } from 'redux'
import { show } from './show'
import { edit } from './edit'

const rootReducer = combineReducers({
  show,
  edit
})

export default rootReducer
