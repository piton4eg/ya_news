import { composeReducer } from 'redux-compose-reducer'

const initialState = {
  loading: false,
  post: {},
  errors: {}
}

const fetchPostBefore = state => ({
  ...state,
  loading: true
})

const fetchPostSuccess = (state, { post }) => ({
  ...state,
  loading: false,
  post
})

const savePostSuccess = (state, { post }) => ({
  ...state,
  errors: {},
  post
})

const savePostFailure = (state, { errors }) => ({
  ...state,
  errors
})

export const edit = composeReducer('edit', {
  fetchPostBefore,
  fetchPostSuccess,
  savePostSuccess,
  savePostFailure
}, initialState)
