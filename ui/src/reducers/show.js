import { composeReducer } from 'redux-compose-reducer'

const initialState = {
  loading: false,
  post: {}
}

const fetchPostBefore = state => ({
  ...state,
  loading: true
})

const fetchPostSuccess = (state, { post }) => ({
  ...state,
  loading: false,
  post
})

export const show = composeReducer('show', {
  fetchPostBefore,
  fetchPostSuccess
}, initialState)
