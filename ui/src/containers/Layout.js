import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AppBar from 'material-ui/AppBar'

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

class Layout extends Component {
  render() {
    return (
      <div>
        <AppBar title="Yandex Main News" showMenuIconButton={ false } />

        <ul>
          <li><Link to="/">Show</Link></li>
          <li><Link to="/admin">Admin</Link></li>
        </ul>

        <hr />

        <div>
          { this.props.children }
        </div>

      </div>
    )
  }
}

export default Layout
