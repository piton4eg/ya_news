import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actions from '../actions/show'
import Post from '../components/Post'

import { ActionCable } from 'react-actioncable-provider'

class Show extends Component {
  componentDidMount() {
    this.props.fetchPost()
  }

  handleRefreshClick = (e) => {
    e.preventDefault()
    this.props.fetchPost()
  }

  onReceived = ({ post }) => {
    this.props.broadcastPost(post)
  }

  render() {
    const { post, loading } = this.props
    const isEmpty = Object.keys(post).length === 0

    return (
      <div>
        <ActionCable
          ref='postChannel'
          channel={{channel: 'PostChannel'}}
          onReceived={this.onReceived}
        />
        { isEmpty
          ? (loading ? <h2>Loading...</h2> : <h2>Fetching news from yandex...</h2>)
          : <div style={{ opacity: loading ? 0.5 : 1 }}>
              <Post post={post} />
            </div>
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { post, loading } = state.show

  return ({
    post,
    loading
  })
}

const mapDispatchToProps = {
  ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Show)
