import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../actions/edit'
import PostForm from '../components/PostForm'

class Edit extends Component {
  componentDidMount() {
    this.props.fetchPost()
  }

  handleSubmit = (post) => {
    this.props.savePost(post)
  }

  render() {
    const { post, loading, errors } = this.props

    return (
      <PostForm
        post={ post }
        loading={ loading }
        errors={ errors }
        onSubmit={ this.handleSubmit }
      />
    )
  }
}

const mapStateToProps = state => {
  const { post, loading, errors } = state.edit

  return ({
    post,
    loading,
    errors
  })
}

const mapDispatchToProps = {
  ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit)
