import React, { Component } from 'react'
import TextField from 'material-ui/TextField'
import TimePicker from 'material-ui/TimePicker'
import RaisedButton from 'material-ui/RaisedButton'

class PostForm extends Component {
  state = {
    id: null,
    title: '',
    annotation: '',
    errors: {},
    main_until: ''
  }

  componentWillMount() {
    this.processProps(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.processProps(nextProps)
  }

  processProps({ post: { id, title, annotation, main_until }, errors }) {
    if (this.state.id !== id) {

      this.setState({
        id,
        title,
        annotation,
        main_until: main_until || new Date()
      })
    }

    if (errors !== this.state.errors) {
      this.setState({ errors })
    }
  }

  handleSubmit = (e) => {
    e.preventDefault()

    const { id, title, annotation, main_until } = this.state
    this.props.onSubmit({ id, title, annotation, main_until })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleChangeMainUntil = (_, date) => {
    this.setState({ main_until: date })
  }

  render() {
    const { title, annotation, main_until, errors } = this.state

    return (
      <div>
        <form
          onSubmit={ this.handleSubmit }
        >
          <TextField
            value={ title }
            errorText={ errors.title }
            onChange={ this.handleChange }
            name="title"
            hintText="Title"
          />
          <br />
          <TextField
            multiLine={ true }
            rows={ 2 }
            value={ annotation }
            errorText={ errors.annotation }
            onChange={ this.handleChange }
            name="annotation"
            hintText="Annotation"
          />
          <br />

          <TimePicker
            value={ new Date(main_until) }
            errorText={ errors.main_until }
            onChange={ this.handleChangeMainUntil }
            autoOk={true}
          />

          <RaisedButton type="submit">Submit</RaisedButton>
        </form>
      </div>
    )
  }
}

export default PostForm
