import React from 'react'

const Post = ({ post }) => (
  <div>
    <h2>{ post.title }</h2>
    <span
      dangerouslySetInnerHTML={{ __html: post.annotation.replace(/(?:\r\n|\r|\n)/g, '<br />') }}
    />
    <br />
    <i>{ post.date || post.created_at }</i>
  </div>
)

export default Post
