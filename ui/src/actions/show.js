import { createTypes } from 'redux-compose-reducer'

const TYPES = createTypes('show', [
  'fetchPostBefore',
  'fetchPostSuccess'
])

export const fetchPost = () => dispatch => {
  dispatch({ type: TYPES.fetchPostBefore })

  return fetch('api/posts/main', { accept: 'application/json' })
    .then(response => response.json())
    .then(json =>
      dispatch({
        type: TYPES.fetchPostSuccess,
        post: json
      })
    )
}

export const broadcastPost = post => dispatch => {
  dispatch({
    type: TYPES.fetchPostSuccess,
    post
  })
}
