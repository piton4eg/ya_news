import { createTypes } from 'redux-compose-reducer'

const TYPES = createTypes('edit', [
  'fetchPostBefore',
  'fetchPostSuccess',
  'savePostSuccess',
  'savePostFailure'
])

export const fetchPost = () => dispatch => {
  dispatch({ type: TYPES.fetchPostBefore })

  return fetch('api/posts/edit', { accept: 'application/json' })
    .then(response => response.json())
    .then(json =>
      dispatch({
        type: TYPES.fetchPostSuccess,
        post: json,
        errors: {}
      })
    )
}

export const savePost = ({ id, title, annotation, main_until }) => dispatch => {
  return fetch(`api/posts/${id ? id : ''}`, {
    method: id ? 'put' : 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ title, annotation, main_until })
  })
    .then(response => response.json())
    .then(json => {
      if (json.errors) {
        dispatch({
          type: TYPES.savePostFailure,
          errors: json.errors
        })
      } else {
        dispatch({
          type: TYPES.savePostSuccess,
          post: json,
          errors: {}
        })
      }
    })
}
