import React from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import ActionCableProvider from 'react-actioncable-provider'

import Layout from './containers/Layout'
import Show from './containers/Show'
import Edit from './containers/Edit'

const AppRouter = () => (
  <Router>
    <MuiThemeProvider>
      <ActionCableProvider url='ws://localhost:3001/cable'>
        <Layout>
          <Route exact path="/" component={Show}/>
          <Route path="/admin" component={Edit}/>
        </Layout>
      </ActionCableProvider>
    </MuiThemeProvider>
  </Router>
)
export default AppRouter
